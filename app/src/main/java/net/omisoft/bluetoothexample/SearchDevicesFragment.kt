package net.omisoft.bluetoothexample

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.tbruyelle.rxpermissions3.RxPermissions
import kotlinx.android.synthetic.main.search_device_fragment.*
import net.omisoft.bluetoothexample.adapters.BluetoothDevicesAdapter
import net.omisoft.bluetoothexample.models.BluetoothStatus
import net.omisoft.bluetoothexample.models.DeviceType
import net.omisoft.bluetoothexample.utils.PermissionUtility

class SearchDevicesFragment : Fragment() {

    private lateinit var bleActivity: BleActivity

    private lateinit var itemDivider: DividerItemDecoration

    private lateinit var rxPermissions: RxPermissions
    private lateinit var handler: Handler
    private val SCAN_PERIOD: Long = 20000
    private var scanning = false

    private val devicesAdapter = BluetoothDevicesAdapter().apply {
        onDeviceClickListener = { bleActivity.onDeviceClicked(it, getSelectedDeviceType()) }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BleActivity) {
            bleActivity = context
        }

        itemDivider = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        rxPermissions = RxPermissions(this)
        handler = Handler(context.mainLooper)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_device_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()
        initBottomNavigation()
        initRecyclerView()
    }

    override fun onStop() {
        rv_devices.removeItemDecoration(itemDivider)
        super.onStop()
    }

    private fun initBottomNavigation() {
        bn_main.setOnNavigationItemSelectedListener {
            val scanningDevicesType = when (it.itemId) {
                R.id.bottom_navigation_all -> DeviceType.ALL
                R.id.bottom_navigation_mi_band -> DeviceType.MI_BAND
                R.id.bottom_navigation_heart_rate -> DeviceType.HEART_RATE_MONITOR
                R.id.bottom_navigation_blood_pressure -> DeviceType.BLOOD_PRESSURE
                R.id.bottom_navigation_phone_alert -> DeviceType.PHONE_ALERT
                else -> DeviceType.ALL
            }

            devicesAdapter.clearDevices()
            scanDevices(scanningDevicesType)
            true
        }
        bn_main.selectedItemId = R.id.bottom_navigation_all
    }

    private fun startScanDevices(deviceType: DeviceType) {
        if (scanning) {
            scanning = false
            progress_circular.visibility = View.GONE
            if (::handler.isInitialized) {
                handler.removeCallbacksAndMessages(null)
            }
            bleActivity.stopSearchDevices()
        }

        progress_circular.visibility = View.VISIBLE
        if (!scanning) { // Stops scanning after a pre-defined scan period.
            handler.postDelayed({
                scanning = false
                progress_circular.visibility = View.GONE
                bleActivity.stopSearchDevices()
            }, SCAN_PERIOD)
            scanning = true
            bleActivity.startSearchDevices(deviceType, ::onBleDeviceFound)
        }
    }

    private fun showBluetoothDisabledError() {
        context?.let {
            AlertDialog.Builder(it).apply {
                setTitle(R.string.bluetooth_turn_off)
                setMessage(R.string.bluetooth_turn_off_description)
                setCancelable(false)
                setPositiveButton(R.string.bluetooth_enable) { _, _ ->
                    val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableBtIntent, RC_ENABLE_BLUETOOTH)
                }
                show()
            }
        }
    }

    private fun showBluetoothNotFoundError() {
        context?.let {
            AlertDialog.Builder(it).apply {
                setTitle(R.string.bluetooth_do_not_support)
                setMessage(R.string.bluetooth_do_not_support_description)
                setCancelable(false)
                setNegativeButton(R.string.done) { _, _ -> }
                show()
            }
        }
    }

    private fun onBleDeviceFound(device: BluetoothDevice) {
        progress_circular.visibility = View.GONE
        if (::handler.isInitialized) {
            scanning = false
            handler.removeCallbacksAndMessages(null)
        }
        devicesAdapter.addDevice(device)
    }

    private fun showTurnOnGpsAlert() {
        context?.let {
            AlertDialog.Builder(it).apply {
                setMessage(R.string.gps_on_reason)
                setCancelable(false)
                setPositiveButton(R.string.settings) { _, _ ->
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, RC_ENABLE_BLUETOOTH)
                }
                show()
            }
        }
    }

    @SuppressLint("CheckResult")
    fun scanDevices(deviceType: DeviceType) {
        val observable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            rxPermissions
                .request(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                )

        } else {
            rxPermissions
                .request(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                )
        }
        observable.subscribe { granted ->
            if (granted) {
                val locationManager =
                    context?.getSystemService(Context.LOCATION_SERVICE) as? LocationManager
                val gpsEnabled =
                    locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        ?: false
                if (gpsEnabled) {

                    when (bleActivity.checkBluetooth()) {
                        BluetoothStatus.ENABLED -> startScanDevices(deviceType)
                        BluetoothStatus.DISABLED -> showBluetoothDisabledError()
                        BluetoothStatus.NOT_FOUND -> showBluetoothNotFoundError()
                    }
                } else {
                    showTurnOnGpsAlert()
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    PermissionUtility.requestPermissions(
                        this, arrayOf(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        ), 1001
                    )

                } else {
                    PermissionUtility.requestPermissions(
                        this, arrayOf(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ), 1001
                    )
                }

                Toast.makeText(
                    context,
                    R.string.cannot_get_location_permission,
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }
    }

    private fun initRecyclerView() {
        rv_devices.layoutManager = LinearLayoutManager(context)
        rv_devices.adapter = devicesAdapter
        rv_devices.addItemDecoration(itemDivider)
    }

    private fun getSelectedDeviceType(): DeviceType =
        when (bn_main.selectedItemId) {
            R.id.bottom_navigation_all -> DeviceType.ALL
            R.id.bottom_navigation_mi_band -> DeviceType.MI_BAND
            R.id.bottom_navigation_heart_rate -> DeviceType.HEART_RATE_MONITOR
            R.id.bottom_navigation_blood_pressure -> DeviceType.BLOOD_PRESSURE
            R.id.bottom_navigation_phone_alert -> DeviceType.PHONE_ALERT
            else -> DeviceType.ALL
        }

    companion object {
        private const val RC_ENABLE_BLUETOOTH = 101

        fun newInstance(): Fragment {
            return SearchDevicesFragment()
        }
    }
}