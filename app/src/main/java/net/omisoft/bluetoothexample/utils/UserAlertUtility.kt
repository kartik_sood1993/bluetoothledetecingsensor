package net.omisoft.bluetoothexample.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.view.ContextThemeWrapper
import net.omisoft.bluetoothexample.R

class UserAlertUtility {
    companion object {
        private var mAlertDialog: AlertDialog? = null

        // Show a dialog alert for some actions
        fun showAlertDialog(
            pTitle: String?,
            pMessage: String?,
            pContext: Context?,
            pOkListener: DialogInterface.OnClickListener? = null,
            pCancelListener: DialogInterface.OnClickListener? = null,
            pPositiveButton: String? = "",
            pNegativeButton: String? = ""
        ) {
            if (pContext != null) {
                /**
                 * If the alert dialog is already showing neednot create a new one
                 */
                if (mAlertDialog == null || !mAlertDialog!!.isShowing) {
                    mAlertDialog =
                        AlertDialog.Builder(
                            ContextThemeWrapper(
                                pContext,
                                R.style.AlertDialogCustom
                            )
                        )
                            .create()
                    if (pTitle == null) {
                        with(mAlertDialog) { this?.setTitle(pContext.getString(R.string.app_name)) }
                    } else {
                        with(mAlertDialog) { this?.setTitle(pTitle) }
                    }
                    mAlertDialog!!.setMessage(pMessage)
                    mAlertDialog!!.setCancelable(false)
                    var positiveText: String? = pContext.getString(R.string.ok_message)
                    mAlertDialog.run {
                        if (pPositiveButton != null && pPositiveButton.isNotEmpty()) {
                            positiveText = pPositiveButton
                        }
                        this!!.setButton(DialogInterface.BUTTON_POSITIVE, positiveText, pOkListener)
                        if ((pNegativeButton != null && pNegativeButton.isNotEmpty()) || pCancelListener != null) {
                            this.setButton(
                                DialogInterface.BUTTON_NEGATIVE,
                                pNegativeButton ?: pContext.getString(R.string.cancel_message),
                                pCancelListener
                            )
                        }
                        show()
                    }
                }
            }
        }

        // hide alert
        fun hideAlertDialog() {
            if (mAlertDialog != null && mAlertDialog!!.isShowing) {
                mAlertDialog!!.dismiss()
                mAlertDialog = null
            }
        }
    }
}