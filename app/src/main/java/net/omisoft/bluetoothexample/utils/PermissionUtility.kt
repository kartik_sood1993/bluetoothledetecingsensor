package net.omisoft.bluetoothexample.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 * Make permission utility for checking permissions
 */
object PermissionUtility {
    private fun useRunTimePermissions(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    fun hasPermission(activity: AppCompatActivity, permission: String): Boolean {
        return if (useRunTimePermissions()) {
            activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    fun hasPermission(activity: Activity, permission: String): Boolean {
        return if (useRunTimePermissions()) {
            activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    fun hasPermissions(activity: AppCompatActivity, permissions: Array<String>): Boolean {
        return if (useRunTimePermissions()) {
            var hasPermission = false
            for (permission in permissions) {
                hasPermission =
                    activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
                if (!hasPermission) {
                    break
                }
            }
            hasPermission
        } else true
    }

    fun hasPermissions(activity: Activity, permissions: Array<String>): Boolean {
        return if (useRunTimePermissions()) {
            var hasPermission = false
            for (permission in permissions) {
                hasPermission =
                    activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
                if (!hasPermission) {
                    break
                }
            }
            hasPermission
        } else true
    }

    fun requestPermission(
        pActivity: AppCompatActivity,
        permission: String,
        requestCode: Int
    ) {
        if (useRunTimePermissions()) {
            pActivity.requestPermissions(arrayOf(permission), requestCode)
        }
    }

    fun requestPermission(
        pActivity: Activity,
        permission: String,
        requestCode: Int
    ) {
        if (useRunTimePermissions()) {
            pActivity.requestPermissions(arrayOf(permission), requestCode)
        }
    }

    fun requestPermissions(
        pActivity: AppCompatActivity,
        permission: Array<String>,
        requestCode: Int
    ) {
        if (useRunTimePermissions()) {
            pActivity.requestPermissions(permission, requestCode)
        }
    }

    fun requestPermissions(
        pActivity: Activity,
        permission: Array<String>,
        requestCode: Int
    ) {
        if (useRunTimePermissions()) {
            pActivity.requestPermissions(permission, requestCode)
        }
    }

    fun requestPermissions(pFragment: Fragment, permission: Array<String>, requestCode: Int) {
        if (useRunTimePermissions()) {
            pFragment.requestPermissions(permission, requestCode)
        }
    }

    fun shouldShowRational(activity: AppCompatActivity, permission: String): Boolean {
        return if (useRunTimePermissions()) {
            activity.shouldShowRequestPermissionRationale(permission)
        } else false
    }

    fun goToAppSettings(activity: AppCompatActivity) {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", activity.packageName, null)
        )
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(intent)
    }

}