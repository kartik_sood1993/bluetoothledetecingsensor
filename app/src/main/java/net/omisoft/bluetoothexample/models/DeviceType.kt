package net.omisoft.bluetoothexample.models

enum class DeviceType {
    ALL, HEART_RATE_MONITOR, BLOOD_PRESSURE,PHONE_ALERT, MI_BAND
}